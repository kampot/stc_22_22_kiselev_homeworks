import model.Product;
import repo.ProductRepository;
import repo.ProductRepositoryFileBasedImpl;

public class Main {
    public static void main(String[] args) {
        ProductRepository productRepository = new ProductRepositoryFileBasedImpl("src/resources/products.txt");

        assert productRepository.findById(2).toLine().equals("2|Сок|85.0|25");
        assert productRepository.findById(13).toLine().equals("13|Соль|34.0|26");
        assert productRepository.findById(6).toLine().equals("6|Варенье|150.0|32");

        assert productRepository.findAllByTitleLike("оло").size() == 2;
        assert productRepository.findAllByTitleLike("оЛо").get(0).toLine().equals("1|Молоко|200.0|10");
        assert productRepository.findAllByTitleLike("ОлО").get(1).toLine().equals("7|Соломка|25.8|21");
        assert productRepository.findAllByTitleLike("рЕц").size() == 2;
        assert productRepository.findAllByTitleLike("Рец").get(0).toLine().equals("9|Огурец|68.0|65");
        assert productRepository.findAllByTitleLike("реЦ").get(1).toLine().equals("11|Перец|45.7|27");
        assert productRepository.findAllByTitleLike("оКа").size() == 0;

        assert productRepository.findById(5).toLine().equals("5|Мед|250.0|60");
        Product honey = productRepository.findById(5);
        honey.setPrice(200.0);
        honey.setCount(61);
        productRepository.update(honey);
        assert productRepository.findById(5).toLine().equals("5|Мед|200.0|61");

        assert productRepository.findById(19).toLine().equals("19|Торт|650.0|25");
        Product cake = productRepository.findById(19);
        cake.setPrice(600.0);
        cake.setCount(30);
        productRepository.update(cake);
        assert productRepository.findById(19).toLine().equals("19|Торт|600.0|30");
    }
}