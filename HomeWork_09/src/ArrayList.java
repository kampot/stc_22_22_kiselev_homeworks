import java.util.Arrays;

/**
 * todo Document type ArrayList
 */
public class ArrayList<T> implements List<T> {

    private final static int DEFAULT_ARRAY_SIZE = 10;
    private T[] elements;
    private int count;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    @Override
    public void add(T element) {
        // если массив заполнен
        if (isFull()) {
            // изменяем его размер
            resize();
        }
        elements[count] = element;
        count++;
    }

    private void resize() {
        int currentLength = elements.length;
        int newLength = currentLength + currentLength / 2;
        T[] newElements = (T[]) new Object[newLength];

        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }

        this.elements = newElements;
    }

    private boolean isFull() {
        return count == elements.length;
    }

    @Override
    public boolean contains(T element) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void remove(T element) {
        try {
            Integer index = null;
            for (int i = 0; i < elements.length; i++) {
                if (element.equals(elements[i])) {
                    index = i;
                    break;
                }
            }
            removeWithArrayCopy(index);
        } catch (Exception e) {

        }


    }

    @Override
    public void removeAt(int index) {
        removeWithArrayCopy(index);
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return null;
    }

    private void removeWithArrayCopy(int index) {
        if (index <= count) {
            T[] copyArray = (T[]) new Object[elements.length - 1];
            System.arraycopy(elements, 0, copyArray, 0, index);
            System.arraycopy(elements, index + 1, copyArray, index, elements.length - index - 1);
            elements = copyArray;
            count--;
        }
    }

    private class ArrayListIterator implements Iterator<T> {

        private int currentIndex = 0;

        @Override
        public T next() {
            T value = elements[currentIndex];
            currentIndex++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < count;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }

}
