

/**
 * todo Document type Main
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> integerList = new LinkedList<>();
        integerList.add(8);
        integerList.add(10);
        integerList.add(11);
        integerList.add(13);
        integerList.add(11);

        integerList.remove(8);
        integerList.remove(11);

        Iterator listIterator = integerList.iterator();

        while (listIterator.hasNext()){
            System.out.print(listIterator.next() + " ");
        }

        System.out.println();

        List<String> stringList = new LinkedList<>();

        stringList.add("Hello");
        stringList.add("How");
        stringList.add("Are");
        stringList.add("You");
        stringList.add("Today");

        stringList.remove("Hello");
        stringList.remove("Today");

        Iterator stringListIterator = stringList.iterator();

        while (stringListIterator.hasNext()){
            System.out.print(stringListIterator.next() + " ");
        }

        System.out.println();

        List<Integer> integerArrayList = new ArrayList<>();

        integerArrayList.add(8);
        integerArrayList.add(10);
        integerArrayList.add(11);
        integerArrayList.add(13);
        integerArrayList.add(11);

        integerArrayList.removeAt(1);
        integerArrayList.remove(11);

        Iterator integerArrayListIterator = integerArrayList.iterator();

        while (integerArrayListIterator.hasNext()){
            System.out.print(integerArrayListIterator.next() + " ");
        }

        System.out.println();

        List<String> stringArrayList = new ArrayList<>();

        stringArrayList.add("Hello");
        stringArrayList.add("How");
        stringArrayList.add("Are");
        stringArrayList.add("You");
        stringArrayList.add("Today");

        stringArrayList.removeAt(0);
        stringArrayList.remove("Today");

        Iterator stringArrayListIterator = stringArrayList.iterator();

        while (stringArrayListIterator.hasNext()){
            System.out.print(stringArrayListIterator.next() + " ");
        }



    }
}
