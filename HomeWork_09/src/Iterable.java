/**
 * todo Document type Iterable
 */
public interface Iterable<T> {
    Iterator<T> iterator();
}

