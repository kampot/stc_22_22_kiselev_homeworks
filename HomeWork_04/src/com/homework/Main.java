package com.homework;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);

        int size;
        int from;
        int to;
        int[] array;

        int numberSize;
        int[] number;

        System.out.println("Enter the size of the array");

        size = scanner.nextInt();

        array = new int[size];

        System.out.println("Enter array elements in quantity:" + size);

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println("Enter Interval \n From");

        from = scanner.nextInt();

        System.out.println("To");

        to = scanner.nextInt();

        int sum = calcSumInInterval(array, from, to);

        System.out.print("Sum in Interval: " + sum + "\n");

        getAllEvenElementsOfArray(array);

        System.out.println("Enter NumberSize");
        numberSize = scanner.nextInt();

        number = new int[numberSize];

        System.out.println("Enter number elements in quantity:" + numberSize);

        for (int i = 0; i < number.length; i++) {
            number[i] = scanner.nextInt();
        }

        int result = toInt(number);

        System.out.println("Your number from NumberArray is: " + result);

    }

    public static int calcSumInInterval(int[] array, int from, int to) {
        int sum = 0;

        if (from < to) {
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            return sum;
        }

        return -1;

    }

    public static void getAllEvenElementsOfArray(int[] array) {
        System.out.println("AllEvenElementsOfArray is: ");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.println(array[i]);
            }
        }
    }

    public static int toInt(int[] number) {

        StringBuilder builder = new StringBuilder();
        int result = 0;

        for (int n : number) {
            builder.append(n);
        }

        try {
            result = Integer.parseInt(builder.toString());
        } catch (Exception e) {
            System.out.println("Can't display value greater than: " + Integer.MAX_VALUE + "\n Or less than: " + Integer.MIN_VALUE);
        }

        return result;

    }
}
