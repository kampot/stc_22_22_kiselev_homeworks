package ru.inno.finalproject.services;

import ru.inno.finalproject.dto.MovieForm;
import ru.inno.finalproject.models.Movie;

import java.util.List;

public interface MovieService {
    List<Movie> getMovies();
    void addMovie(MovieForm movieForm);
    Movie getMovie(Long id);
    void updateMovie(Long movieId, MovieForm movie);
    void deleteMovie(Long movieId);
}
