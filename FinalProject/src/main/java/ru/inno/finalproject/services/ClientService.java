package ru.inno.finalproject.services;

import ru.inno.finalproject.dto.ClientForm;
import ru.inno.finalproject.models.Client;

import java.util.List;

public interface ClientService {
    List<Client> getClients();
    void addClient(ClientForm clientForm);
    Client getClient(Long id);
    void updateClient(Long clientId, ClientForm client);
    void deleteClient(Long clientId);
}
