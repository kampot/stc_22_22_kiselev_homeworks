package ru.inno.finalproject.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.finalproject.dto.MovieForm;
import ru.inno.finalproject.models.Movie;
import ru.inno.finalproject.repo.MovieRepository;
import ru.inno.finalproject.services.MovieService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;

    @Override
    public List<Movie> getMovies() {
        return movieRepository.findAll();
    }

    @Override
    public void addMovie(MovieForm movieForm) {
        Movie newMovie = Movie.builder()
                .name(movieForm.getName())
                .genre(movieForm.getGenre())
                .description(movieForm.getDescription())
                .ageRestrictions(movieForm.getAgeRestrictions())
                .duration(movieForm.getDuration())
                .build();

        movieRepository.save(newMovie);
    }

    @Override
    public Movie getMovie(Long id) {
        return movieRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateMovie(Long movieId, MovieForm movie) {
        Movie movieForUpdate = movieRepository.findById(movieId).orElseThrow();

        movieForUpdate.setName(movie.getName());
        movieForUpdate.setDescription(movie.getDescription());
        movieForUpdate.setGenre(movie.getGenre());
        movieForUpdate.setDuration(movie.getDuration());
        movieForUpdate.setAgeRestrictions(movie.getAgeRestrictions());

        movieRepository.save(movieForUpdate);
    }

    @Override
    public void deleteMovie(Long movieId) {
        movieRepository.delete(movieRepository.findById(movieId).orElseThrow());
    }
}
