package ru.inno.finalproject.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.inno.finalproject.dto.ClientForm;
import ru.inno.finalproject.models.Client;
import ru.inno.finalproject.repo.ClientRepository;
import ru.inno.finalproject.services.SignUp;

@Service
@RequiredArgsConstructor
public class SignUpImpl implements SignUp {
    private final ClientRepository clientRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void signUp(ClientForm clientForm) {
        Client newClient = Client.builder()
                .username(clientForm.getUsername())
                .firstName(clientForm.getFirstName())
                .lastName(clientForm.getLastName())
                .email(clientForm.getEmail())
                .password(passwordEncoder.encode(clientForm.getPassword()))
                .state(Client.State.CONFIRMED)
                .role(Client.Role.USER)
                .age(clientForm.getAge())
                .build();

        clientRepository.save(newClient);
    }

}
