package ru.inno.finalproject.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.finalproject.dto.SessionForm;
import ru.inno.finalproject.models.CinemaHall;
import ru.inno.finalproject.models.Client;
import ru.inno.finalproject.models.Movie;
import ru.inno.finalproject.models.Session;
import ru.inno.finalproject.repo.CinemaHallRepository;
import ru.inno.finalproject.repo.ClientRepository;
import ru.inno.finalproject.repo.MovieRepository;
import ru.inno.finalproject.repo.SessionRepository;
import ru.inno.finalproject.security.details.CustomUserDetails;
import ru.inno.finalproject.services.SessionService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class SessionServiceImpl implements SessionService {

    private final SessionRepository sessionRepository;
    private final MovieRepository movieRepository;
    private final CinemaHallRepository cinemaHallRepository;

    @Override
    public List<Session> getSessions() {
        return sessionRepository.findAll();
    }

    @Override
    public void addSession(SessionForm session) {
        Session newSession = Session.builder()
                .showDate(LocalDateTime.parse(session.getShowDate()))
                .movie(movieRepository.findMovieByName(session.getMovieName().trim()).orElseThrow())
                .cinemaHall(cinemaHallRepository.findById(session.getCinemaHall()).orElseThrow())
                .ticketCount(session.getTicketCount())
                .build();

        sessionRepository.save(newSession);
    }

    @Override
    public Session getSession(Long id) {
        return sessionRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateSession(Long sessionId, SessionForm session) {
        Session sessionForUpdate = sessionRepository.findById(sessionId).orElseThrow();

        sessionForUpdate.setMovie(movieRepository.findMovieByName(session.getMovieName().trim()).orElseThrow());
        sessionForUpdate.setCinemaHall(cinemaHallRepository.findById(session.getCinemaHall()).orElseThrow());
        sessionForUpdate.setShowDate(LocalDateTime.parse(session.getShowDate()));

        sessionRepository.save(sessionForUpdate);
    }

    @Override
    public void deleteSession(Long sessionId) {
        sessionRepository.delete(sessionRepository.findById(sessionId).orElseThrow());
    }

    @Override
    public void buyTicket(Long sessionId, CustomUserDetails userDetails) {
        Client client = userDetails.getClient();
        Session session = sessionRepository.findById(sessionId).orElseThrow();
        session.getClients().add(client);
        session.setTicketCount(session.getTicketCount() - 1);
        sessionRepository.save(session);
    }

    @Override
    public void deleteTicket(Long sessionId, CustomUserDetails userDetails) {
        Client client = userDetails.getClient();
        Session session = sessionRepository.findById(sessionId).orElseThrow();
        session.getClients().remove(client);
        session.setTicketCount(session.getTicketCount() + 1);
        sessionRepository.save(session);
    }
}
