package ru.inno.finalproject.services;

import ru.inno.finalproject.models.Client;
import ru.inno.finalproject.security.details.CustomUserDetails;

public interface ProfileService {
    Client getCurrent(CustomUserDetails userDetails);
}
