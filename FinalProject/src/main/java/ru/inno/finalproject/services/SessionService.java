package ru.inno.finalproject.services;

import ru.inno.finalproject.dto.SessionForm;
import ru.inno.finalproject.models.Session;
import ru.inno.finalproject.security.details.CustomUserDetails;

import java.util.List;

public interface SessionService {
    List<Session> getSessions();
    void addSession(SessionForm session);
    Session getSession(Long id);
    void updateSession(Long sessionId, SessionForm session);
    void deleteSession(Long sessionId);

    void buyTicket(Long sessionId, CustomUserDetails userDetails);

    void deleteTicket(Long sessionId, CustomUserDetails userDetails);
}
