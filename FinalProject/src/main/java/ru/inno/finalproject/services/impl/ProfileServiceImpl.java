package ru.inno.finalproject.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.finalproject.models.Client;
import ru.inno.finalproject.repo.ClientRepository;
import ru.inno.finalproject.security.details.CustomUserDetails;
import ru.inno.finalproject.services.ProfileService;

@Service
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {

    private final ClientRepository clientRepository;

    @Override
    public Client getCurrent(CustomUserDetails userDetails) {
        return clientRepository.findById(userDetails.getClient().getId()).orElseThrow();
    }
}
