package ru.inno.finalproject.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.finalproject.dto.ClientForm;
import ru.inno.finalproject.models.Client;
import ru.inno.finalproject.repo.ClientRepository;
import ru.inno.finalproject.services.ClientService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Override
    public List<Client> getClients() {
        return clientRepository.findAll();
    }

    @Override
    public void addClient(ClientForm client) {
        Client newClient = Client.builder()
                .username(client.getUsername())
                .password(client.getPassword())
                .email(client.getEmail())
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .age(client.getAge())
                .state(Client.State.CONFIRMED)
                .role(Client.Role.USER)
                .build();

        clientRepository.save(newClient);
    }

    @Override
    public Client getClient(Long id) {
        return clientRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateClient(Long clientId, ClientForm client) {
        Client clientForUpdate = clientRepository.findById(clientId).orElseThrow();

        clientForUpdate.setFirstName(client.getFirstName());
        clientForUpdate.setLastName(client.getLastName());
        clientForUpdate.setAge(client.getAge());

        clientRepository.save(clientForUpdate);

    }

    @Override
    public void deleteClient(Long clientId) {
        clientRepository.delete(clientRepository.findById(clientId).orElseThrow());
    }
}
