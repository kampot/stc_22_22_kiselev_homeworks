package ru.inno.finalproject.services;

import ru.inno.finalproject.dto.ClientForm;

public interface SignUp {
    void signUp(ClientForm clientForm);
}
