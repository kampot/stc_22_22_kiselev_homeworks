package ru.inno.finalproject.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.finalproject.dto.MovieForm;
import ru.inno.finalproject.services.MovieService;

@RequiredArgsConstructor
@Controller
public class MovieController {
    private final MovieService movieService;

    @GetMapping("/movies")
    public String getMovies(Model model) {
        model.addAttribute("movies", movieService.getMovies());
        return "movies/movies";
    }

    @PostMapping("/movies")
    public String addMovie(MovieForm movie) {
        movieService.addMovie(movie);
        return "redirect:/movies";
    }

    @GetMapping("/movies/{movieId}")
    public String getMovie(@PathVariable("movieId") Long id, Model model) {
        model.addAttribute("movie", movieService.getMovie(id));
        return "movies/movie";
    }

    @PostMapping("/movies/{movieId}/update")
    public String updateMovie(@PathVariable("movieId") Long movieId, MovieForm movie) {
        movieService.updateMovie(movieId, movie);
        return "redirect:/movies/" + movieId;
    }

    @PostMapping("/movies/{movieId}/delete")
    public String deleteMovie(@PathVariable("movieId") Long movieId) {
        movieService.deleteMovie(movieId);
        return "redirect:/movies";
    }
}
