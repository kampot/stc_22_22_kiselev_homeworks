package ru.inno.finalproject.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.finalproject.dto.ClientForm;
import ru.inno.finalproject.services.ClientService;

@RequiredArgsConstructor
@Controller
public class ClientController {

    private final ClientService clientService;

    @GetMapping("/clients")
    public String getClients(Model model) {
        model.addAttribute("clients", clientService.getClients());
        return "clients/clients";
    }

    @GetMapping("/clients/{clientId}")
    public String getClient(@PathVariable("clientId") Long id, Model model) {
        model.addAttribute("client", clientService.getClient(id));
        return "clients/client";
    }

    @PostMapping("/clients/{clientId}/update")
    public String updateClient(@PathVariable("clientId") Long clientId, ClientForm client) {
        clientService.updateClient(clientId, client);
        return "redirect:/clients/" + clientId;
    }

    @PostMapping("/clients/{clientId}/delete")
    public String deleteClient(@PathVariable("clientId") Long clientId) {
        clientService.deleteClient(clientId);
        return "redirect:/clients";
    }

}

