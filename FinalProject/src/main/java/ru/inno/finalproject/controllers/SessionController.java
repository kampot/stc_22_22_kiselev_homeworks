package ru.inno.finalproject.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.finalproject.dto.SessionForm;
import ru.inno.finalproject.security.details.CustomUserDetails;
import ru.inno.finalproject.services.SessionService;

@RequiredArgsConstructor
@Controller
public class SessionController {
    private final SessionService sessionService;


    @GetMapping("/sessions")
    public String getSessions(Model model) {
        model.addAttribute("sessions", sessionService.getSessions());
        return "sessions/sessions";
    }

    @PostMapping("/sessions")
    public String addSession(SessionForm session) {
        sessionService.addSession(session);
        return "redirect:/sessions";
    }

    @GetMapping("/sessions/{sessionId}")
    public String getSession(@PathVariable("sessionId") Long id, Model model) {
        model.addAttribute("session", sessionService.getSession(id));
        return "sessions/session";
    }

    @PostMapping("/sessions/{sessionId}/update")
    public String updateSession(@PathVariable("sessionId") Long sessionId, SessionForm session) {
        sessionService.updateSession(sessionId, session);
        return "redirect:/sessions/" + sessionId;
    }

    @PostMapping("/sessions/{sessionId}/delete")

    public String deleteSession(@PathVariable("sessionId") Long sessionId) {
        sessionService.deleteSession(sessionId);
        return "redirect:/sessions";
    }

    @PostMapping("/sessions/{sessionId}/buyTicket")
    public String buyTicket(@PathVariable("sessionId") Long sessionId,
                            @AuthenticationPrincipal CustomUserDetails userDetails) {
        sessionService.buyTicket(sessionId, userDetails);
        return "redirect:/profile";
    }

    @PostMapping("/sessions/{sessionId}/deleteTicket")
    public String deleteTicket(@PathVariable("sessionId") Long sessionId,
                               @AuthenticationPrincipal CustomUserDetails userDetails) {
        sessionService.deleteTicket(sessionId, userDetails);
        return "redirect:/profile";
    }
}
