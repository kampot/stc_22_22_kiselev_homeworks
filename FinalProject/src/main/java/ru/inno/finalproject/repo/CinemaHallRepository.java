package ru.inno.finalproject.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.finalproject.models.CinemaHall;

public interface CinemaHallRepository extends JpaRepository<CinemaHall, Long> {
}
