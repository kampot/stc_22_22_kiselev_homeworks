package ru.inno.finalproject.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.finalproject.models.Movie;

import java.util.Optional;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    Optional<Movie> findMovieByName(String name);
}
