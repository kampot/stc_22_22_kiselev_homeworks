package ru.inno.finalproject.repo;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.finalproject.models.Session;

import java.util.List;

public interface SessionRepository extends JpaRepository<Session, Long> {
}
