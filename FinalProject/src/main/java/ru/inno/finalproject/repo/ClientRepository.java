package ru.inno.finalproject.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.finalproject.models.Client;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findByUsername(String username);
}
