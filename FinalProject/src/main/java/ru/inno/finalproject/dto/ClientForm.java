package ru.inno.finalproject.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientForm {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Integer age;
}
