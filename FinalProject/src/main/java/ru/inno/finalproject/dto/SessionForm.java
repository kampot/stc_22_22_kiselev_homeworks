package ru.inno.finalproject.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SessionForm {
    private String movieName;
    private Long cinemaHall;
    private String showDate;
    private Integer ticketCount;
}
