package ru.inno.finalproject.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovieForm {
    private String name;
    private String description;
    private String genre;
    private Integer ageRestrictions;
    private String duration;
}
