package ru.inno.finalproject.services.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.inno.finalproject.dto.SessionForm;
import ru.inno.finalproject.models.CinemaHall;
import ru.inno.finalproject.models.Movie;
import ru.inno.finalproject.models.Session;
import ru.inno.finalproject.repo.CinemaHallRepository;
import ru.inno.finalproject.repo.MovieRepository;
import ru.inno.finalproject.repo.SessionRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class SessionServiceImplTest {

    private static final long EXISTED_SESSION_ID = 1L;


    private SessionServiceImpl sessionService;
    private SessionRepository sessionRepository;
    private MovieRepository movieRepository;
    private CinemaHallRepository cinemaHallRepository;

    private final Session EXISTED_SESSION = Session.builder()
            .id(1L)
            .showDate(LocalDateTime.of(2022, 12, 10, 11, 20))
            .cinemaHall(getTestCinemaHall(5L, "2D"))
            .movie(getMovie(4L, 14, "desc", "3", "mult", "NONAME"))
            .ticketCount(28)
            .build();

    List<Session> EXISTED_SESSIONS = List.of(
            Session.builder()
                    .id(1L)
                    .showDate(LocalDateTime.of(2022, 12, 6, 10,00))
                    .cinemaHall(getTestCinemaHall(1L, "2D"))
                    .movie(getMovie(1L, 16, "Description", "2 часа", "Комедия", "Имя"))
                    .ticketCount(30)
                    .build(),
            Session.builder()
                    .id(2L)
                    .showDate(LocalDateTime.of(2022, 12, 7, 12, 30))
                    .cinemaHall(getTestCinemaHall(2L, "3D"))
                    .movie(getMovie(2L, 18, "Desc", "3", "fight", "Yeee"))
                    .ticketCount(25)
                    .build(),
            Session.builder()
                    .id(3L)
                    .showDate(LocalDateTime.of(2022, 12, 8, 11, 40))
                    .cinemaHall(getTestCinemaHall(3L, "2D"))
                    .movie(getMovie(3L, 12, "desc", "1", "com", "ice"))
                    .ticketCount(12)
                    .build());

    SessionForm EXISTED_SESSION_FORM = SessionForm.builder()
            .movieName("Гринч")
            .cinemaHall(2L)
            .showDate(LocalDateTime.of(2022, 12, 9, 10, 25).toString())
            .ticketCount(20)
            .build();

    @BeforeEach
    void setUp() {
        setUpMocks();

        stubMocks();

        this.sessionService = new SessionServiceImpl(sessionRepository, movieRepository, cinemaHallRepository);
    }

    private void setUpMocks() {
        this.sessionRepository = Mockito.mock(SessionRepository.class);
        this.movieRepository = Mockito.mock(MovieRepository.class);
        this.cinemaHallRepository = Mockito.mock(CinemaHallRepository.class);
    }

    private void stubMocks() {
        when(sessionRepository.findAll()).thenReturn(EXISTED_SESSIONS);
        when(sessionRepository.findById(EXISTED_SESSION_ID)).thenReturn(Optional.of(EXISTED_SESSION));

        when(movieRepository.findMovieByName(EXISTED_SESSION_FORM.getMovieName()))
                .thenReturn(Optional.of(getMovie(4L, 0, "HY", "2", "com", "Гринч")));
        when(cinemaHallRepository.findById(EXISTED_SESSION_FORM.getCinemaHall()))
                .thenReturn(Optional.of(getTestCinemaHall(2L, "3D")));
    }



    @Test
    void getSessions() {
        List<Session> actual = sessionRepository.findAll();

        List<Session> expected = EXISTED_SESSIONS;

        assertEquals(expected, actual);
    }

    @Test
    void addSession() {
        sessionService.addSession(EXISTED_SESSION_FORM);
        verify(sessionRepository).save(Session.builder()
                        .movie(movieRepository.findMovieByName(EXISTED_SESSION_FORM.getMovieName()).orElseThrow())
                        .showDate(LocalDateTime.parse(EXISTED_SESSION_FORM.getShowDate()))
                        .cinemaHall(cinemaHallRepository.findById(EXISTED_SESSION_FORM.getCinemaHall()).orElseThrow())
                        .ticketCount(EXISTED_SESSION_FORM.getTicketCount())
                        .build());
    }

    @Test
    void getSession() {
        Session expected = sessionRepository.findById(EXISTED_SESSION_ID).orElseThrow();

        Session actual = EXISTED_SESSION;

        assertEquals(expected, actual);

    }

    private CinemaHall getTestCinemaHall(Long id, String name) {
        return CinemaHall.builder()
                .id(id)
                .name(name)
                .build();
    }

    private Movie getMovie(Long id, Integer ageRestrictions,
                           String description, String duration,
                           String genre, String name){
        return Movie.builder()
                .id(id)
                .name(name)
                .description(description)
                .genre(genre)
                .duration(duration)
                .ageRestrictions(ageRestrictions)
                .build();
    }
}