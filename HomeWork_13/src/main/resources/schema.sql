create table car
(
    id        bigserial primary key,
    model     varchar(30),
    color     varchar(15),
    number    varchar(10)
);