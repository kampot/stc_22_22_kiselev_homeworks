package ru.inno;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.models.Car;
import ru.inno.repo.CarsRepository;
import ru.inno.repo.CarsRepositoryImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;
import java.util.Scanner;

@Parameters(separators = "=")
public class Main {
    @Parameter(names = {"-action"})
    private Action action;

    public static void main(String[] args) {
        Main program = new Main();

        JCommander.newBuilder()
                .addObject(program)
                .build()
                .parse(args);


        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Objects.requireNonNull(Main.class.getResourceAsStream("/db.properties")))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        CarsRepository carsRepository = new CarsRepositoryImpl(dataSource);

        if (program.action.name().equals("read")) {
            carsRepository.findAll().forEach(System.out::println);
        }

        if (program.action.name().equals("write")) {
            String command = "";

            try (Scanner scanner = new Scanner(System.in)) {
                while (! command.equals("exit")) {
                    System.out.println("Model");
                    String model = scanner.nextLine();
                    System.out.println("Color");
                    String color = scanner.nextLine();
                    System.out.println("Number");
                    String number = scanner.nextLine();
                    Car car = Car.builder()
                            .model(model)
                            .color(color)
                            .number(number)
                            .build();

                    carsRepository.save(car);

                    System.out.println("Type exit or enter");
                    command = scanner.nextLine();
                }
            }
        }
    }
}