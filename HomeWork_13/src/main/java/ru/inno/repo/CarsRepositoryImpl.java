package ru.inno.repo;

import ru.inno.models.Car;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarsRepositoryImpl implements CarsRepository {
    private static final String SQL_SELECT_ALL = "select * from car order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into car(model, color, number) " +
            "values (?, ?, ?)";

    private final DataSource dataSource;

    public CarsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (ResultSet resultSet = dataSource.getConnection()
                .createStatement()
                .executeQuery(SQL_SELECT_ALL)) {
            while (resultSet.next()) {
                Car car = parseCar(resultSet);
                cars.add(car);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return cars;
    }

    @Override
    public void save(Car car) {

        try (PreparedStatement preparedStatement = dataSource.getConnection()
                .prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, car.getModel());
            preparedStatement.setString(2, car.getColor());
            preparedStatement.setString(3, car.getNumber());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert car");
            }
            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    car.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't obtain generated id");
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    private Car parseCar(ResultSet resultSet) {
        try {
            return Car.builder()
                    .id(resultSet.getLong("id"))
                    .model(resultSet.getString("model"))
                    .color(resultSet.getString("color"))
                    .number(resultSet.getString("number"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
