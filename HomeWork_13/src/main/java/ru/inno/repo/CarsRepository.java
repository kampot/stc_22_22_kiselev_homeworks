package ru.inno.repo;

import ru.inno.models.Car;

import java.util.List;

public interface CarsRepository {
    List<Car> findAll();

    void save(Car car);
}
