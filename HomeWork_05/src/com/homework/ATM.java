package com.homework;

public class ATM {

    private Integer maxAmountAllowedForWithdrawal;

    private Integer maxValueInATM;

    private Integer amountOfMoneyLeft;

    private Integer operationCount;

    public ATM(Integer maxAmountAllowedForWithdrawal, Integer maxValueInATM, Integer amountOfMoneyLeft, Integer operationCount) {
        this.maxAmountAllowedForWithdrawal = maxAmountAllowedForWithdrawal;
        this.maxValueInATM = maxValueInATM;
        this.amountOfMoneyLeft = amountOfMoneyLeft;
        this.operationCount = operationCount;
    }

    public String getMoney(int amount) {
        if (amount < maxAmountAllowedForWithdrawal && amount < amountOfMoneyLeft) {
            amountOfMoneyLeft -= amount;

            operationCount++;

            return "Выдана сумма: " + amount;
        } else {
            return "Сумма превышает размер, разрешенный к выдаче, или остаток средств в банкомате.";
        }
    }

    public String putMoney(int amount) {
        if (amount + amountOfMoneyLeft < maxValueInATM ) {
            amountOfMoneyLeft += amount;

            operationCount++;

            return "Операция выполнена успешна";
        } else {
            return "Невозможно положить " + amount + " , введите сумму меньше " + (maxValueInATM - amountOfMoneyLeft);
        }
    }

    public Integer getOperationCount() {
        return operationCount;
    }
}
