package com.homework;

public class Main {

    public static void main(String[] args) {

        ATM sberBank = new ATM(1000000,
                20000000, 10000000, 0);

        sberBank.getMoney(1000);

        sberBank.putMoney(36000);

        sberBank.getMoney(15000000);

        sberBank.putMoney(9000000);

        sberBank.putMoney(2000000);

        assert sberBank.getOperationCount().equals(3);
    }
}
