import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        String words = "Cheered name discovered few uneasy danger like weeks home ecstatic walk Friend among increasing" +
                " Secure cordially marianne smart worthy happen could your elinor placing differed four convinced graceful thoroughly " +
                "Side related herself expenses child except boy should pleased declared six " +
                "Offending fact invitation offered inquiry real distrusts favour not real " +
                "Confined spoil match insipidity reserved roof draw determine" +
                " Reached stairs attended temper Order entrance which misery better grave with advantage gravity" +
                " Apartments followed me means. Good this entire distant carried" +
                " Vanity linen smiling ever musical sociable something solicitude others end dining" +
                " Doors figure result help sufficient offered suffering mind admitting ignorant comfort understood" +
                " Figure prudent saved jokes certainly innate depend affronting four" +
                " Welcomed building least provision preserved possible wonder remainder ";

        String testWords = "Hello Hello bye Hello bye Inno";
        String testWords2 = "Kirill Hello How Are You Kirill Bye";
        String testWords3 = "Hello Hello Hello How Are Are You Bye Bye Inno Inno Inno Inno";



        assert Map.of("Hello", 3).equals(wordCountWithCorrectOutputInMap(testWords));
        assert Map.of("Kirill", 2).equals(wordCountWithCorrectOutputInMap(testWords2));
        assert Map.of("Inno", 4).equals(wordCountWithCorrectOutputInMap(testWords3));

        assert "Hello=3".equals(wordCountsWithStringOutput(testWords));
        assert "Kirill=2".equals(wordCountsWithStringOutput(testWords2));
        assert "Inno=4".equals(wordCountsWithStringOutput(testWords3));

        assert "four=2".equals(wordCountsWithStringOutput(words));
        assert Map.of("four", 2).equals(wordCountWithCorrectOutputInMap(words));



    }

    public static Map<String, Integer> wordCountWithCorrectOutputInMap(String words){
        String[] wordsArray = words.split(" ");
        Arrays.sort(wordsArray);

        Map<String, Integer> wordMap = new HashMap<>();

        String maxWord = "";
        String word = "";
        int maxCount = 0;
        int count = 1;

        for (String arrayWord : wordsArray) {
            if (arrayWord.equals(word)) {
                count++;
            } else {
                if (count > maxCount) {
                    maxCount = count;
                    maxWord = word;
                }
                word = arrayWord;
                count = 1;
            }
        }

        if (count > maxCount) {
            maxCount = count;
            maxWord = word;
        }

            wordMap.put(maxWord, maxCount);

        return wordMap;
    }

    public static String wordCountsWithStringOutput(String words) {
        Map<String, Integer> wordMap = new HashMap<>();
        String[] wordsArray = words.split(" ");
        int count = 0;

        for (String word : wordsArray){
            for (String wordNext : wordsArray) {
                if (word.equals(wordNext)) {
                    count++;
                }
            }
            wordMap.put(word, count);
            count = 0;
        }

        return wordMap.entrySet()
                .stream()
                .max(Comparator.comparingInt(Map.Entry::getValue))
                .get()
                .toString();

    }

    public static Map<String, Long> wordsCountsProMentorMethod(String words) {
        return Arrays.stream(words.split(" "))
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()));
    }
}