package com.homework;

/**
 * todo Document type ArrayTask
 */
public interface ArrayTask {
    int resolve(int[] array, int from, int to);
}
