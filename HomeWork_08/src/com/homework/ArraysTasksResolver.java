package com.homework;

import java.util.Arrays;

/**
 * todo Document type ArraysTasksResolver
 */
public class ArraysTasksResolver {
    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println(Arrays.toString(array));
        System.out.println(from + " " + to);
        System.out.println(task.resolve(array, from, to));
    }

    public static int resolveForTest(int[] array, ArrayTask task, int from, int to){
        return task.resolve(array, from, to);
    }
}
