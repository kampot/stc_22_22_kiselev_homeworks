package com.homework;

public class Main {

    public static void main(String[] args) {

        ArraysTasksResolver.resolveTask(new int[] {12, 62, 4, 2, 100, 40, 56}, (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            return sum;
        }, 2, 3);

        ArraysTasksResolver.resolveTask(new int[] {12, 62, 4, 2, 100, 40, 56}, (array, from, to) -> {
            int max = 0;
            for (int i = from; i <= to ; i++) {
                if (max < array[i]) {
                    max = array[i];
                }
            }
            return max / 10 + max % 10;
        }, 1, 3);

        assertionTestMethod();
    }

    public static void assertionTestMethod(){
        assert ArraysTasksResolver.resolveForTest(new int[]{3, 4, 12, 45, 2, 7}, (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            return sum;
        }, 0, 3) == 64;

        assert ArraysTasksResolver.resolveForTest(new int[]{3, 4, 12, 45, 2, 7}, (array, from, to) -> {
            int max = 0;
            for (int i = from; i <= to ; i++) {
                if (max < array[i]) {
                    max = array[i];
                }
            }
            return max / 10 + max % 10;
        }, 0, 3) == 9;

    }
}
