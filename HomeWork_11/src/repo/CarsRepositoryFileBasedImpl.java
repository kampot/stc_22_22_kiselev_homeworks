package repo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import model.Car;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Car> findAll() {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return bufferedReader
                    .lines()
                    .map(CarsRepositoryFileBasedImpl::parseCar)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> findAllCarNumbersByColorOrMileage(String color, int mileage) {
        return findAll().stream()
                .filter(car -> car.color().equals(color) || car.mileage().equals(mileage))
                .map(Car::number)
                .toList();
    }

    @Override
    public Integer findUniqueModelCountWithPriceRange(int from, int to) {
        if (from < to) {
            return findAll().stream()
                    .filter(car -> car.price() >= from && car.price() <= to)
                    .map(Car::model)
                    .distinct()
                    .toList()
                    .size();
        } else {
            return null;
        }
    }

    @Override
    public String findCarColorWithMinPrice() {
        return findAll().stream()
                .min(Comparator.comparing(Car::price))
                .orElseThrow()
                .color();
    }

    @Override
    public Double findAveragePricePerModel(String model) {
        return findAll().stream()
                .filter(car -> car.model().equals(model))
                .mapToDouble(Car::price)
                .average()
                .orElseThrow();

    }

    private static Car parseCar(String cars) {
        String[] carFields = cars.split("\\|");
        return new Car(carFields[0], carFields[1], carFields[2],
                Integer.valueOf(carFields[3]), Integer.valueOf(carFields[4]));
    }
}
