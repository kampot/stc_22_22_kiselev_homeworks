package repo;

import java.util.List;

import model.Car;

public interface CarsRepository {
    List<Car> findAll();

    List<String> findAllCarNumbersByColorOrMileage(String color, int mileage);

    Integer findUniqueModelCountWithPriceRange(int from, int to);

    String findCarColorWithMinPrice();

    Double findAveragePricePerModel(String model);
}
