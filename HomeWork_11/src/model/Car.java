package model;

public record Car(String number, String model, String color, Integer mileage, Integer price) {}
