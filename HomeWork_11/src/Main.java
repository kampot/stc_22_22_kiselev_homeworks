import java.util.List;
import repo.CarsRepository;
import repo.CarsRepositoryFileBasedImpl;


public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("cars.txt");

        assert List.of("A111AA777", "O000OO190", "A011AA777", "A201AA777", "O333OO190")
                .equals(carsRepository.findAllCarNumbersByColorOrMileage("Black", 0));
        assert List.of("A001AA777", "A111AA777", "A101AA777", "X437TB750", "A277BT199")
                .equals(carsRepository.findAllCarNumbersByColorOrMileage("Gray", 40));

        assert carsRepository.findUniqueModelCountWithPriceRange(700000, 800000).equals(3);
        assert carsRepository.findUniqueModelCountWithPriceRange(1500000, 3000000).equals(4);
        assert carsRepository.findUniqueModelCountWithPriceRange(1500000, 250000) == null;

        assert carsRepository.findCarColorWithMinPrice().equals("Pink");

        assert carsRepository.findAveragePricePerModel("Camry").equals(2500000.0);
        assert carsRepository.findAveragePricePerModel("Civic").equals(625000.0);
    }
}