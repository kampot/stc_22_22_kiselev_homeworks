package com.homework;

/**
 * todo Document type Square
 */
public class Square extends Figure{

    private double a;

    public Square(int x, int y, int a) {
        super(x, y);
        this.a = a;
    }

    public double getA() {
        return a;
    }

    @Override
    public double getPerimeter() {
        return a * 4;
    }

    @Override
    public double getSquare() {
        return a * a;
    }

    @Override
    public void move(int toX, int toY) {
        super.x += toX;
        super.y += toY;
    }
}
