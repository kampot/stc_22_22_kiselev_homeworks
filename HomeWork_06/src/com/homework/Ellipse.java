package com.homework;

/**
 * todo Document type Ellipse
 */
public class Ellipse extends Figure{

    int smallRadius;

    int bigRadius;

    public Ellipse(int x, int y, int smallRadius, int bigRadius) {
        super(x, y);
        this.smallRadius = smallRadius;
        this.bigRadius = bigRadius;
    }

    public int getSmallRadius() {
        return smallRadius;
    }

    public int getBigRadius() {
        return bigRadius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt((Math.pow(bigRadius, 2) + Math.pow(smallRadius, 2)) / 2);
    }

    @Override
    public double getSquare() {
        return Math.PI * bigRadius * smallRadius;
    }

    @Override
    public void move(int toX, int toY) {
        super.x += toX;
        super.y += toY;
    }

}
