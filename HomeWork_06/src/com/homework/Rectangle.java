package com.homework;

/**
 * todo Document type Rectangle
 */
public class Rectangle extends Figure{

    private double height;
    private double width;

    public Rectangle(int x, int y, int height, int width) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public double getPerimeter() {
        return (width + height) * 2;
    }

    @Override
    public double getSquare() {
        return width * height;
    }

    @Override
    public void move(int toX, int toY) {
        super.x += toX;
        super.y += toY;
    }
}
