package com.homework;

/**
 * todo Document type Сircle
 */
public class Circle extends Figure{

    private double radius;

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double getSquare() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public void move(int toX, int toY) {
        super.x += toX;
        super.y += toY;
    }
}
