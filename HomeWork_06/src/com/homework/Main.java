package com.homework;

public class Main {

    public static void main(String[] args) {

        assert new Rectangle(4, 3, 5, 2).getPerimeter() == 14;
        assert new Rectangle(4, 3, 5, 2).getSquare() == 10;
        assert new Rectangle(4, 3, 5, 2).getHeight() == 5;
        assert new Rectangle(4, 3, 5, 2).getWidth() == 2;

        Rectangle rectangle = new Rectangle(4, 3, 5, 2);
        rectangle.move(1, 2);
        assert rectangle.x == 5;
        assert rectangle.y == 5;

        assert new Square(5, 5, 5).getPerimeter() == 20;
        assert new Square(5, 5, 5).getSquare() == 25;
        assert new Square(5, 5, 5).getA() == 5;

        Square square = new Square(5, 5, 5);
        square.move(3, 7);
        assert square.x == 8;
        assert square.y == 12;

        assert new Circle(4, 7, 5).getPerimeter() == 2 * Math.PI * 5;
        assert new Circle(4, 7, 5).getSquare() == Math.PI * 25;
        assert new Circle(4, 7, 5).getRadius() == 5;

        Circle circle = new Circle(4, 7, 5);
        circle.move(2, 3);
        assert circle.x == 6;
        assert circle.y == 10;

        assert new Ellipse(4, 4, 2, 6).getPerimeter() == 2 * Math.PI * (Math.sqrt((36 + 4) / 2));
        assert new Ellipse(4, 4, 2, 6).getSquare() == Math.PI * 12;
        assert new Ellipse(4, 4, 2, 6).getSmallRadius() == 2;
        assert new Ellipse(4, 4, 2, 6).getBigRadius() == 6;

        Ellipse ellipse = new Ellipse(4, 4, 2, 6);
        ellipse.move(2, 2);
        assert ellipse.x == 6;
        assert ellipse.y == 6;
    }
}
