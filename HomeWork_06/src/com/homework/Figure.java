package com.homework;

/**
 * todo Document type Figure
 */
public abstract class Figure {
    int x;
    int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract double getPerimeter();

    public abstract double getSquare();

    public abstract void move(int toX, int toY);
}
