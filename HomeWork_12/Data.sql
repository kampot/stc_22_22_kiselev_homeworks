insert into client (first_name, last_name, phone_number, experience, age, has_a_drivers_license,
                    drivers_license_category, rating)
values ('Кирилл', 'Киселев', '79777773047', 4, 22, true, 'B', 5),
       ('Сергей', 'Петров', '79653211475', 2, 25, true, 'B', 4.3),
       ('Светлана', 'Копилкина', '79856423174', 1, 36, true, 'B', 3.4),
       ('Мария', 'Арсентьева', '79036430112', 7, 29, true, 'B', 4.7),
       ('Максим', 'Курнаков', '79160851624', 3, 23, true, 'B', 3.8);

insert into car (model, color, number, client_id)
values ('Toyota Camry', 'Черный', 'A777AA77', 5),
       ('Honda Civic', 'Белый', 'X467TB750', 1),
       ('BMW 320d', 'Серый', 'K137TY190', 2),
       ('Audi A1', 'Красный', 'A111MM199', 3),
       ('Nissan Micro', 'Зеленый', 'X125TC90', 4);

insert into trip (client_id, car_id, trip_date, trip_duration)
values (1, 3, '2022-10-31', 4),
       (2, 1, '2022-11-01', 12),
       (3, 4, '2022-10-06', 10),
       (4, 2, '2022-09-03', 5),
       (5, 1, '2022-11-13', 9);