create table client
(
    id                       bigserial primary key,
    first_name               varchar(20) not null,
    last_name                varchar(20) not null,
    phone_number             varchar(20) not null,
    experience               integer     not null,
    age                      integer     not null,
    has_license              bool,
    drivers_license_category varchar(10) not null,
    rating                   double precision
);

create table car
(
    id        bigserial primary key,
    model     varchar(30),
    color     varchar(15),
    number    varchar(10),
    owner_id  bigint not null,
    foreign key (owner_id) references client (id)
);

create table trip
(
    client_id     bigint not null,
    car_id        bigint not null,
    trip_date     timestamp,
    trip_duration integer,
    foreign key (client_id) references client (id),
    foreign key (car_id) references car (id)
);

