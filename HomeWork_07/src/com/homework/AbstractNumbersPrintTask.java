package com.homework;

/**
 * todo Document type AbstractNumbersPrintTask
 */
public abstract class AbstractNumbersPrintTask implements Task{
    int from;
    int to;

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }
}
