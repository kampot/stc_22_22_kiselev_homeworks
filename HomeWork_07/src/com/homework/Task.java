package com.homework;

import java.util.List;

/**
 * todo Document type Task
 *
 */
public interface Task {

    List<Integer> calc();
    default void complete(){
        System.out.println(calc());
    }
}
