package com.homework;

import java.util.ArrayList;
import java.util.List;

/**
 * todo Document type EvenNumbersPrintTask
 */
public class EvenNumbersPrintTask extends AbstractNumbersPrintTask{

    public EvenNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public List<Integer> calc() {
        List<Integer> numbersList = new ArrayList<>();
        for (int i = from; i <= to ; i++) {
            if (i % 2 == 0) {
                numbersList.add(i);
            }
        }
        return numbersList;
    }
}
