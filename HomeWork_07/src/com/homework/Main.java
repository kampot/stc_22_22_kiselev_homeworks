package com.homework;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        assert List.of(2, 4, 6, 8, 10).equals(new EvenNumbersPrintTask(2, 10).calc());
        assert List.of(2, 4).equals(new EvenNumbersPrintTask(1, 5).calc());
        assert List.of(10, 12, 14, 16).equals(new EvenNumbersPrintTask(10, 17).calc());
        assert List.of(4, 6, 8, 10, 12).equals(new EvenNumbersPrintTask(3, 12).calc());

        assert List.of(3, 5, 7).equals(new OddNumbersPrintTask(3, 8).calc());
        assert List.of(1, 3, 5).equals(new OddNumbersPrintTask(1, 5).calc());
        assert List.of(11, 13, 15, 17, 19).equals(new OddNumbersPrintTask(10, 19).calc());
        assert List.of(5, 7, 9, 11, 13, 15).equals(new OddNumbersPrintTask(5, 16).calc());

        Task[] tasks = {new EvenNumbersPrintTask(2, 10), new OddNumbersPrintTask(3, 8), new EvenNumbersPrintTask(1, 5),
                        new OddNumbersPrintTask(1, 23)};
        completeAllTasks(tasks);
    }

    public static void completeAllTasks(Task[] tasks){
        for (Task task : tasks) {
            task.complete();
        }
    }
}
