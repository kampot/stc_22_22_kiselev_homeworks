package com.homework;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int size;

        int count = 0;

        System.out.println("Enter the size of the array");

        size = scanner.nextInt();

        int[] array = new int[size];

        System.out.println("Enter array elements in quantity: " + size);

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        if ((array[0] < array[1]) || array[size - 1] < array[size - 2]) {
            count++;
        }

        for (int i = 1; i < array.length - 1; i++) {
            if (array[i - 1] > array[i] && array[i] < array[i + 1]) {
                count++;
            }
        }

        System.out.println(count);



    }
}
